/*
	
	var _cbox = new CBox(
			new THREE.Vector3(0,1,0),
			new THREE.Vector3(0,0,1),
			new THREE.Vector3(10,40,0),
			10,
			10,
			10,
			10,
			10,
			10,
			);

*/
CBox = function(  
		vunit_up,
		vunit_front,
		origin,
		width_left,
		width_right,
		height_top,
		height_bottom,
		thickness_front,
		thickness_back
){

	this._vX = new THREE.Vector3(1,0,0);
	this._vY = new THREE.Vector3(0,1,0);
	this._vZ = new THREE.Vector3(0,0,1);
	
	this.vunit_up = vunit_up.clone().normalize();
	this.vunit_front = vunit_front.clone().normalize();
	this.origin = origin.clone();
	this.last_origin = origin.clone();
	this.width_left = width_left;
	this.width_right = width_right;
	this.height_top = height_top;
	this.height_bottom = height_bottom;
	this.thickness_front = thickness_front;
	this.thickness_back = thickness_back;
	this.calc();
	this.velocity = 0;
	this.gravity = 10;


	this._tmpLineAB = new THREE.Line3();
	this._tmpLineBC = new THREE.Line3();
	this._tmpLineCD = new THREE.Line3();
	this._tmpLineDA = new THREE.Line3();

	this._tmpLineEF = new THREE.Line3();
	this._tmpLineFG = new THREE.Line3();
	this._tmpLineGH = new THREE.Line3();
	this._tmpLineHE = new THREE.Line3();

	this._tmpLineAE = new THREE.Line3();
	this._tmpLineBF = new THREE.Line3();
	this._tmpLineCG = new THREE.Line3();
	this._tmpLineDH = new THREE.Line3();

	this._tmpPlaneUp 		= new THREE.Plane(new THREE.Vector3(0,1,0),1);
	this._tmpPlaneBottom 	= new THREE.Plane(new THREE.Vector3(0,1,0),1);
	this._tmpPlaneLeft 		= new THREE.Plane(new THREE.Vector3(0,1,0),1);
	this._tmpPlaneRight 	= new THREE.Plane(new THREE.Vector3(0,1,0),1);
	this._tmpPlaneFront 	= new THREE.Plane(new THREE.Vector3(0,1,0),1);
	this._tmpPlaneBack 		= new THREE.Plane(new THREE.Vector3(0,1,0),1);

	this._tmpPlaneUp.tri1   	= new THREE.Triangle();
	this._tmpPlaneUp.tri2   	= new THREE.Triangle();

	this._tmpPlaneBottom.tri1   = new THREE.Triangle();
	this._tmpPlaneBottom.tri2   = new THREE.Triangle();

	this._tmpPlaneLeft.tri1   	= new THREE.Triangle();
	this._tmpPlaneLeft.tri2   	= new THREE.Triangle();

	this._tmpPlaneRight.tri1   	= new THREE.Triangle();
	this._tmpPlaneRight.tri2   	= new THREE.Triangle();

	this._tmpPlaneFront.tri1   	= new THREE.Triangle();
	this._tmpPlaneFront.tri2   	= new THREE.Triangle();

	this._tmpPlaneBack.tri1   	= new THREE.Triangle();
	this._tmpPlaneBack.tri2   	= new THREE.Triangle();


	this.pointA 	= new THREE.Vector3();//A
	this.pointB 	= new THREE.Vector3();//B
	this.pointC 	= new THREE.Vector3();//C
	this.pointD 	= new THREE.Vector3();//D

	this.pointE 	= new THREE.Vector3();//E
	this.pointF 	= new THREE.Vector3();//F
	this.pointG 	= new THREE.Vector3();//G
	this.pointH 	= new THREE.Vector3();//H

}
CBox.prototype = {
	calc:function (){
		this.vunit_left = this.vunit_front.clone().cross(this.vunit_up).normalize();
		this.vunit_front.copy(this.vunit_left).applyAxisAngle(this.vunit_up,Math.PI/2).normalize();
	

		this.vertices = new Float32Array(108);
		this.buffer = new THREE.BufferGeometry();
		this.pointsGeometry = new THREE.Geometry();
		this.material = new THREE.MeshBasicMaterial( { color: 0xff0000,transparent:true,opacity:0.3 } );
		this.lineMaterial = new THREE.LineBasicMaterial( {
			color: 0x00ff00,
			linewidth: 1,
			linecap: 'round', //ignored by WebGLRenderer
			linejoin:  'round', //ignored by WebGLRenderer 
		} );
		this.pointsMaterial = new THREE.PointsMaterial( { color: 0x888888,size:2 } );
		this.mesh = new THREE.Mesh(this.buffer,this.material); 
		this.line = new THREE.Line(this.buffer,this.lineMaterial); 
		this.points = new THREE.Points( this.pointsGeometry, this.pointsMaterial );
		this._setPoints();

		var loader = new THREE.FontLoader();
		var _this  = this;
		this.fonts = {
			TLF:{mesh:null,point:this.pointA},
			TRF:{mesh:null,point:this.pointB},
			TRB:{mesh:null,point:this.pointC},
			TLB:{mesh:null,point:this.pointD},
			BLF:{mesh:null,point:this.pointE},
			BRF:{mesh:null,point:this.pointF},
			BRB:{mesh:null,point:this.pointG},
			BLB:{mesh:null,point:this.pointH},
		};

		loader.load( 'fonts/helvetiker_regular.typeface.json', function ( font ) { 

			for(var _idx in _this.fonts){

				(
					function(_k,_this){ 
						_text = new THREE.TextBufferGeometry(_k, {
							font: font,
							size: 0.8,
							height: 1.3,
							curveSegments: 8,
							bevelEnabled: false,
							bevelThickness: 1,
							bevelSize: 1.65,
							bevelSegments: 0
						});
						console.log(_text); 
						_this.fonts[_k].mesh = new THREE.Mesh(_text, new THREE.MeshBasicMaterial( { color: 0x00ff00 } ));
					}
				)(_idx,_this);

			}

		} );

	},
	_setPoints:function(){
		this.pointA.copy(this.origin)
		.addScaledVector(this.vunit_up,this.height_top)
		.addScaledVector(this.vunit_left,this.width_left)
		.addScaledVector(this.vunit_front,this.thickness_front);

		this.pointD.copy(this.origin)
		.addScaledVector(this.vunit_up,this.height_top)
		.addScaledVector(this.vunit_left,this.width_left)
		.addScaledVector(this.vunit_front,-this.thickness_back);

		this.pointB.copy(this.origin)
		.addScaledVector(this.vunit_up,this.height_top)
		.addScaledVector(this.vunit_left,-this.width_right)
		.addScaledVector(this.vunit_front,this.thickness_front);

		this.pointC.copy(this.origin)
		.addScaledVector(this.vunit_up,this.height_top)
		.addScaledVector(this.vunit_left,-this.width_right)
		.addScaledVector(this.vunit_front,-this.thickness_back);

		this.pointE.copy(this.origin)
		.addScaledVector(this.vunit_up,-this.height_bottom)
		.addScaledVector(this.vunit_left,this.width_left)
		.addScaledVector(this.vunit_front,this.thickness_front);

		this.pointH.copy(this.origin)
		.addScaledVector(this.vunit_up,-this.height_bottom)
		.addScaledVector(this.vunit_left,this.width_left)
		.addScaledVector(this.vunit_front,-this.thickness_back);

		this.pointF.copy(this.origin)
		.addScaledVector(this.vunit_up,-this.height_bottom)
		.addScaledVector(this.vunit_left,-this.width_right)
		.addScaledVector(this.vunit_front,this.thickness_front);

		this.pointG.copy(this.origin)
		.addScaledVector(this.vunit_up,-this.height_bottom)
		.addScaledVector(this.vunit_left,-this.width_right)
		.addScaledVector(this.vunit_front,-this.thickness_back);

		this.pointsGeometry.vertices.splice(0,this.pointsGeometry.vertices.length);
		this.pointsGeometry.vertices.push(this.pointA);
		this.pointsGeometry.vertices.push(this.pointB);
		this.pointsGeometry.vertices.push(this.pointC);
		this.pointsGeometry.vertices.push(this.pointD);
		this.pointsGeometry.vertices.push(this.pointE);
		this.pointsGeometry.vertices.push(this.pointF);
		this.pointsGeometry.vertices.push(this.pointG);
		this.pointsGeometry.vertices.push(this.pointH);

		this._tmpLineAB.start 	= this.pointA;
		this._tmpLineAB.end 	= this.pointB;

		this._tmpLineBC.start 	= this.pointB;
		this._tmpLineBC.end 	= this.pointC;

		this._tmpLineCD.start 	= this.pointC;
		this._tmpLineCD.end 	= this.pointD;

		this._tmpLineDA.start 	= this.pointD;
		this._tmpLineDA.end 	= this.pointA;

		this._tmpLineEF.start 	= this.pointE;
		this._tmpLineEF.end 	= this.pointF;

		this._tmpLineFG.start 	= this.pointF;
		this._tmpLineFG.end 	= this.pointG;

		this._tmpLineGH.start 	= this.pointG;
		this._tmpLineGH.end 	= this.pointH;

		this._tmpLineHE.start 	= this.pointH;
		this._tmpLineHE.end 	= this.pointE;

		this._tmpLineAE.start 	= this.pointA;
		this._tmpLineAE.end 	= this.pointE;

		this._tmpLineBF.start 	= this.pointB;
		this._tmpLineBF.end 	= this.pointF;

		this._tmpLineCG.start 	= this.pointC;
		this._tmpLineCG.end 	= this.pointG;

		this._tmpLineDH.start 	= this.pointD;
		this._tmpLineDH.end 	= this.pointH;

		this._tmpPlaneUp.normal.copy(this.vunit_up);
		this._tmpPlaneUp.constant = this._tmpPlaneUp.normal.dot(this.pointA);
		this._tmpPlaneUp.p0 = this.pointA;
		this._tmpPlaneUp.tri1.a = this.pointA;
		this._tmpPlaneUp.tri1.b = this.pointB;
		this._tmpPlaneUp.tri1.c = this.pointC;
		this._tmpPlaneUp.tri2.a = this.pointC;
		this._tmpPlaneUp.tri2.b = this.pointD;
		this._tmpPlaneUp.tri2.c = this.pointA;
		
		this._tmpPlaneBottom.normal.copy(this.vunit_up).negate();
		this._tmpPlaneBottom.constant = this._tmpPlaneBottom.normal.dot(this.pointE);
		this._tmpPlaneBottom.p0 = this.pointE; 
		this._tmpPlaneBottom.tri1.a = this.pointH;
		this._tmpPlaneBottom.tri1.b = this.pointG;
		this._tmpPlaneBottom.tri1.c = this.pointF;
		this._tmpPlaneBottom.tri2.a = this.pointF;
		this._tmpPlaneBottom.tri2.b = this.pointE;
		this._tmpPlaneBottom.tri2.c = this.pointH;

		this._tmpPlaneLeft.normal.copy(this.vunit_left);
		this._tmpPlaneLeft.constant = this._tmpPlaneLeft.normal.dot(this.pointE);
		this._tmpPlaneLeft.p0 = this.pointE;
		this._tmpPlaneLeft.tri1.a = this.pointH;
		this._tmpPlaneLeft.tri1.b = this.pointE;
		this._tmpPlaneLeft.tri1.c = this.pointA;
		this._tmpPlaneLeft.tri2.a = this.pointA;
		this._tmpPlaneLeft.tri2.b = this.pointD;
		this._tmpPlaneLeft.tri2.c = this.pointH;

		this._tmpPlaneRight.normal.copy(this.vunit_left).negate();
		this._tmpPlaneRight.constant = this._tmpPlaneRight.normal.dot(this.pointF);
		this._tmpPlaneRight.p0 = this.pointF;
		this._tmpPlaneRight.tri1.a = this.pointF;
		this._tmpPlaneRight.tri1.b = this.pointG;
		this._tmpPlaneRight.tri1.c = this.pointC;
		this._tmpPlaneRight.tri2.a = this.pointC;
		this._tmpPlaneRight.tri2.b = this.pointB;
		this._tmpPlaneRight.tri2.c = this.pointF;

		this._tmpPlaneFront.normal.copy(this.vunit_front);
		this._tmpPlaneFront.constant = this._tmpPlaneFront.normal.dot(this.pointF);
		this._tmpPlaneFront.p0 = this.pointF;
		this._tmpPlaneFront.tri1.a = this.pointE;
		this._tmpPlaneFront.tri1.b = this.pointF;
		this._tmpPlaneFront.tri1.c = this.pointB;
		this._tmpPlaneFront.tri2.a = this.pointB;
		this._tmpPlaneFront.tri2.b = this.pointA;
		this._tmpPlaneFront.tri2.c = this.pointE;


		this._tmpPlaneBack.normal.copy(this.vunit_front).negate();
		this._tmpPlaneBack.constant = this._tmpPlaneBack.normal.dot(this.pointG);
		this._tmpPlaneBack.p0 = this.pointG;
		this._tmpPlaneBack.tri1.a = this.pointG;
		this._tmpPlaneBack.tri1.b = this.pointH;
		this._tmpPlaneBack.tri1.c = this.pointD;
		this._tmpPlaneBack.tri2.a = this.pointD;
		this._tmpPlaneBack.tri2.b = this.pointC;
		this._tmpPlaneBack.tri2.c = this.pointG;

		var _arr = [];

		var _squareTop = [
			this.pointA.x,this.pointA.y,this.pointA.z,
			this.pointB.x,this.pointB.y,this.pointB.z,
			this.pointC.x,this.pointC.y,this.pointC.z,

			this.pointC.x,this.pointC.y,this.pointC.z,
			this.pointD.x,this.pointD.y,this.pointD.z,
			this.pointA.x,this.pointA.y,this.pointA.z,
		];
		_arr.push(_squareTop);

		var _squareBottom = [
			this.pointH.x,this.pointH.y,this.pointH.z,
			this.pointG.x,this.pointG.y,this.pointG.z,
			this.pointF.x,this.pointF.y,this.pointF.z,

			this.pointF.x,this.pointF.y,this.pointF.z,
			this.pointE.x,this.pointE.y,this.pointE.z,
			this.pointH.x,this.pointH.y,this.pointH.z,
		];
		_arr.push(_squareBottom);

		var _squareFront = [
			this.pointE.x,this.pointE.y,this.pointE.z,
			this.pointF.x,this.pointF.y,this.pointF.z,
			this.pointB.x,this.pointB.y,this.pointB.z,

			this.pointB.x,this.pointB.y,this.pointB.z,
			this.pointA.x,this.pointA.y,this.pointA.z,
			this.pointE.x,this.pointE.y,this.pointE.z,
		];
		_arr.push(_squareFront);

		var _squareBack = [
			this.pointD.x,this.pointD.y,this.pointD.z,
			this.pointC.x,this.pointC.y,this.pointC.z,
			this.pointG.x,this.pointG.y,this.pointG.z,

			this.pointG.x,this.pointG.y,this.pointG.z,
			this.pointH.x,this.pointH.y,this.pointH.z,
			this.pointD.x,this.pointD.y,this.pointD.z,
		];
		_arr.push(_squareBack);

		var _squareLeft = [
			this.pointH.x,this.pointH.y,this.pointH.z,
			this.pointE.x,this.pointE.y,this.pointE.z,
			this.pointA.x,this.pointA.y,this.pointA.z,

			this.pointA.x,this.pointA.y,this.pointA.z,
			this.pointD.x,this.pointD.y,this.pointD.z,
			this.pointH.x,this.pointH.y,this.pointH.z,
		];
		_arr.push(_squareLeft);

		var _squareRight = [
			this.pointF.x,this.pointF.y,this.pointF.z,
			this.pointG.x,this.pointG.y,this.pointG.z,
			this.pointC.x,this.pointC.y,this.pointC.z,

			this.pointC.x,this.pointC.y,this.pointC.z,
			this.pointB.x,this.pointB.y,this.pointB.z,
			this.pointF.x,this.pointF.y,this.pointF.z,
		];
		_arr.push(_squareRight);

		var _tmp = [];

		for(var _idx in _arr){
			for(var _idy in _arr[_idx]){
				_tmp.push(_arr[_idx][_idy]);
			} 
		}
		console.log(_tmp);
		this.vertices.set(_tmp);  
		delete _tmp;
		delete _arr;
		var _position_attribute = this.buffer.getAttribute('position');
		if(!_position_attribute){
			this.buffer.addAttribute( 'position', new THREE.BufferAttribute( this.vertices, 3 ) );
			this.mesh.geometry = this.buffer;
		}else{
			_position_attribute.setArray(this.vertices);
			_position_attribute.needsUpdate = true; 
			this.points.geometry.verticesNeedUpdate = true;

			for(_idx in this.fonts){
				var _font = this.fonts[_idx];
				_font.mesh.position.copy(_font.point); 
			}
		}
		

	},
	rotX: function(angle,needsupdate){
		this.vunit_up.applyAxisAngle(this._vX,angle).normalize();
		this.vunit_front.applyAxisAngle(this._vX,angle).normalize();
		this.vunit_left.applyAxisAngle(this._vX,angle).normalize();
		if(needsupdate)this._setPoints();

	},
	rotY: function(angle,needsupdate){
		this.vunit_up.applyAxisAngle(this._vY,angle).normalize();
		this.vunit_front.applyAxisAngle(this._vY,angle).normalize();
		this.vunit_left.applyAxisAngle(this._vY,angle).normalize();
		if(needsupdate)this._setPoints();
	},
	rotZ: function(angle,needsupdate){
		this.vunit_up.applyAxisAngle(this._vZ,angle).normalize();
		this.vunit_front.applyAxisAngle(this._vZ,angle).normalize();
		this.vunit_left.applyAxisAngle(this._vZ,angle).normalize();
		if(needsupdate)this._setPoints();
	},
	rot:function(angleX,angleY,angleZ){
		var _euler = new THREE.Euler( angleX,angleY,angleZ, 'XYZ' );
		this.vunit_up.applyEuler(_euler).normalize();
		this.vunit_front.applyEuler(_euler).normalize();
		this.vunit_left.applyEuler(_euler).normalize(); 
		this._setPoints();
	},
	attachToScene:function(scene){
		if(this.attached)return;
		this.attached = true;
		scene.add(this.mesh);
		scene.add(this.line);
		scene.add(this.points);
		for(_idx in this.fonts){
			var _font = this.fonts[_idx];
			_font.mesh.position.copy(_font.point);
			scene.add(_font.mesh);
		}
		var _this = this;
		setInterval(function(){ 
			for(_idx in _this.fonts){
				var _font = _this.fonts[_idx];
				_font.mesh.lookAt(w_wgl.camera.position);
			}
		},100);
	},
} 