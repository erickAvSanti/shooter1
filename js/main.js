$(window).ready(
	function(evt){  
		setCanvasDim();
		$(window).resize(
			function(evt){
				setCanvasDim();
			}
		);
		(window.appMain = appgame.Main).start();
	}
); 
function setCanvasDim(){
	window.w_width = window.innerWidth;
	window.w_height = window.innerHeight;
	$("canvas").each(
		function(index,obj){
			var _this = $(obj);
			var _id = _this.attr("id");
			if(
				_id!="webgl" &&  
				_id!="canvas-assets" &&  
				_id!="canvas-touch" 
			){
				return;
			}
			_this.css("width",w_width+"px").css("height",w_height+"px");
			_this.attr("width",w_width).attr("height",w_height);
		}
	);
}

var appgame = appgame || {};
appgame.Main = (
	function(){

		var _prevent_fall = false;

		var _instance = null;

		var _router = null;
		var _zona_a = null;
 		var axesHelper = new THREE.AxesHelper( 15 );
		var _ambientLight = new THREE.AmbientLight( 0xeeeeee); 
		var _directionalLight = new THREE.DirectionalLight( 0xaaaaaa, 0.1 );
		var _directionalLight2 = new THREE.DirectionalLight( 0xaaaaaa, 0.3 );
		var _directionalLight3 = new THREE.DirectionalLight( 0xaaaaaa, 0.3 );

		var _cameraZPosInit = -1.16643;
		var _cameraYPosInit = 31;
		var _cameraYPosInit_min = 0;
		var _cameraXPosInit = 1;

		var _cameraAngle_def = 60;
		var _cameraAngle = _cameraAngle_def;

		var _fullScreen = null;
		var _orbitCamera = null;
		var _cameraControl = null;
		var _bloques = null;
		var _stats = null;

		_debug_touch = true;

		Main = function(){ 

			window.w_wgl 		= document.getElementById("webgl");
 			window.w_cassets 	= document.getElementById("canvas-assets");
 			window.w_ctouch 	= document.getElementById("canvas-touch");
 			if(window.w_cassets && window.w_cassets.getContext)window.w_cassets_ctx = window.w_cassets.getContext("2d");
 			if(window.w_ctouch && window.w_ctouch.getContext)window.w_ctouch_ctx = window.w_ctouch.getContext("2d");

		};
		Main.prototype = {
			constructor:Main,
			start:function(){
				var scene = new THREE.Scene();
				var camera = new THREE.PerspectiveCamera( _cameraAngle, w_width / w_height, 0.1, 1000 );

				var renderer = new THREE.WebGLRenderer({canvas:w_wgl});
				renderer.setSize(w_width,w_height);
				renderer.setPixelRatio(window.devicePixelRatio);
  
  				_directionalLight2.position.set(-200,0,200); 
  				_directionalLight2.target = new THREE.Object3D();
  				_directionalLight2.target.position.set(0,0,0);
  
  				_directionalLight3.position.set(200,0,-200); 
  				_directionalLight3.target = new THREE.Object3D();
  				_directionalLight3.target.position.set(0,0,0);

				scene.add( axesHelper );

				scene.add( _ambientLight );
				scene.add( _directionalLight );
				scene.add( _directionalLight2 );
				scene.add( _directionalLight2.target );
				scene.add( _directionalLight3 );
				scene.add( _directionalLight3.target );
				camera.position.set(_cameraXPosInit,_cameraYPosInit,_cameraZPosInit);
				camera.lookAt(new THREE.Vector3());
				scene.background = new THREE.Color(0.1,0.1,0.1);
				w_wgl.scene = scene;
				w_wgl.camera = camera;
				w_wgl.renderer = renderer; 
				w_wgl.playing = false;
				w_wgl.rqsAnim = true;

				w_wgl._lib_currTime = Date.now();
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
 

				var _this = this;
				$(window).resize(
					function(evt){
						_this.resize();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				);
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);

				w_ctouch.addEventListener("touchstart",_this.touchstart,false);
				w_ctouch.addEventListener("touchend",_this.touchend,false);
				w_ctouch.addEventListener("touchcancel",_this.touchcancel,false);
				w_ctouch.addEventListener("touchleave",_this.touchleave,false);
				w_ctouch.addEventListener("touchmove",_this.touchmove,false);

				_stats = new Stats();
				_stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
				document.body.appendChild( _stats.dom );

				_this.animate();

				_zona_a = appgame.ZonaA.instance(); 
				(_fullScreen = appgame.FullScreen.instance()).start(); 
				(_orbitCamera = appgame.OrbitCamera.instance()).start(); 
				(_cameraControl = appgame.CameraControl.instance()).start();
				(_bloques = appgame.Bloques.instance()).start();

				_this.mostrarPaneldeBienvenida();
				_this.procesarNivelJuego();

				_this.ctrlButton(); 

			},
			ctrlButton:function(){
				$("body").append("<div id='settings'></div>"); 
				$("#settings").click(
					function(_evt){ 
						$("#modal_bienvenida").modal("show");
					}
				);
			},
			touchstart:function(_evt){ 
				if(_orbitCamera)_orbitCamera.touch("s",_evt);
				if(_cameraControl)_cameraControl.touch("s",_evt);
			},
			touchend:function(_evt){ 
				if(_orbitCamera)_orbitCamera.touch("e",_evt);
				if(_cameraControl)_cameraControl.touch("e",_evt);
			},
			touchcancel:function(_evt){ 
				if(_orbitCamera)_orbitCamera.touch("c",_evt);
				if(_cameraControl)_cameraControl.touch("c",_evt);
			},
			touchleave:function(_evt){ 
				if(_orbitCamera)_orbitCamera.touch("l",_evt);
				if(_cameraControl)_cameraControl.touch("l",_evt);
			},
			touchmove:function(_evt){ 
				if(_orbitCamera)_orbitCamera.touch("m",_evt);
				if(_cameraControl)_cameraControl.touch("m",_evt);
			}, 
			animate:function(){
				if(w_wgl.rqsAnim){
					w_wgl.rqs_anim_id = requestAnimationFrame( _instance.animate );
				}  
				_stats.begin();
				if(!w_wgl._lib_currTime){
					w_wgl._lib_currTime = Date.now();
					w_wgl._lib_lastTime = w_wgl._lib_currTime; 
				}
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
				w_wgl._lib_currTime = Date.now();

				if(w_ctouch_ctx)w_ctouch_ctx.clearRect(0,0,w_width,w_height);
				if(_fullScreen)_fullScreen.render();
				if(_orbitCamera)_orbitCamera.render();
				if(_cameraControl)_cameraControl.render();
				if(_bloques)_bloques.render();
				if(_zona_a)_zona_a.render();
				
				if(w_wgl.camera.position.y<_cameraYPosInit_min){
					if(_prevent_fall)w_wgl.camera.position.y = _cameraYPosInit_min;
				}

				w_wgl.renderer.render(w_wgl.scene, w_wgl.camera);
				_stats.end();
			},
			blur:function(){
				//this.stopAnimation();
			},
			focus:function(){
				this.playAnimation();
			}, 
			stopAnimation:function(){
				w_wgl.rqsAnim = false;
				if(w_wgl.rqs_anim_id){
					cancelAnimationFrame(w_wgl.rqs_anim_id);
				}
			},
			playAnimation:function(){
				if(w_wgl.rqs_anim_id)cancelAnimationFrame(w_wgl.rqs_anim_id);
				w_wgl.rqsAnim = true;
				this.animate();
			},
			resize:function(){
				w_wgl.camera.aspect = w_width / w_height;
    			w_wgl.camera.updateProjectionMatrix();
				w_wgl.renderer.setSize(w_width,w_height);
				if(_fullScreen)_fullScreen.resize();
				if(_orbitCamera)_orbitCamera.resize();
				if(_cameraControl)_cameraControl.resize();
			},
			mostrarPaneldeBienvenida:function(){
				$("#modal_bienvenida").modal("show");
				if(window.location && !/localhost/.test(window.location.hostname)){
					var _str = ''+
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>'+
						'<ins class="adsbygoogle"'+
						     'style="display:block;"'+
						     'data-ad-format="fluid"'+
						     'data-ad-layout-key="-8m+w-aj+dk+bi"'+
						     'data-ad-client="ca-pub-6209186190244737"'+
						     'data-ad-slot="7742196875"></ins>'+
						'<script>'+
						     '(adsbygoogle = window.adsbygoogle || []).push({});'+
						'</script>';

					var _str2 = ''+
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>'+
						'<ins class="adsbygoogle"'+
					     'style="display:block;"'+
					     'data-ad-format="fluid"'+
					     'data-ad-layout-key="-8n+x-ah+dn+as"'+
					     'data-ad-client="ca-pub-6209186190244737"'+
					     'data-ad-slot="8801742078"></ins>'+
						'<script>'+
						     '(adsbygoogle = window.adsbygoogle || []).push({});'+
						'</script>'; 

					$("#ct-1").html(_str);
					$("#ct-2").html(_str2); 
				}
			},
			procesarNivelJuego:function(){
				var _this = this;
				AppRouter = Backbone.Router.extend({	    
				    initialize: function() { 
				    },
				    routes: {
				        "": "zonaA",
				        "zona-A": "zonaA",
				        "zona-B": "zonaB",
				        "zona-C": "zonaC"
				    }, 
				    zonaA: function () { 
				    	_this.cancelarCargaActual();
				    	_this.cargarZonaA();
				    }, 
				    zonaB: function () { 
				    	_this.cancelarCargaActual();
				    	_this.cargarZonaA();
				    }, 
				    zonaC: function () { 
				    	_this.cancelarCargaActual();
				    	_this.cargarZonaA();
				    },
				    _routeToRegExp: function (route) {
				        route = route.replace(this.escapeRegExp, "\\$&")
				               .replace(this.namedParam, "([^\/]*)")
				               .replace(this.splatParam, "(.*?)");

				        return new RegExp('^' + route + '$', 'i'); // Just add the 'i'
				    } 
				}); 
				_router = new AppRouter();
		    	Backbone.history.start();
			},
			cancelarCargaActual:function(){
				this.eliminarObjetosDeEscena();
			},
			eliminarObjetosDeEscena:function(){
				if(w_wgl.scene){ 
				} 
			},
			cargarZonaA:function(){
				if(_zona_a)_zona_a.cargarElementos();
			}
		};

		return {
			start:function(){
				if(!_instance){
					_instance = new Main();
					_instance.start();
				}
			}
		};
	}
)();
