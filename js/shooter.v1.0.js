var appgame = appgame || {};

appgame.Shooter = (
	function(){

		var _instance = null;  

		var _balls = [];
		var _ball_velocity = 50/1000;

		var _ball_instance = undefined;

		var _ball_distance_camera = 2;

		var _dt = 20/1000;

		var _parent = undefined;//CameraControl

		Shooter = function(_p){
			_parent = _p;
		};
		Shooter.prototype = {
			constructor:Shooter, 
			test:function(){
				console.log("hello from Shooter");
			},
			start:function(){  
				if(w_wgl.scene){
					var geometry = new THREE.SphereGeometry( 0.2, 8, 8 );
					var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
					material.opacity = 0.2;
					material.transparent = true;
					var sphere = new THREE.Mesh( geometry, material ); 
					_ball_instance = sphere;
				} 
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					$(w_ctouch).click(
						function(_evt){
							_this.listen(_evt);
						}
					);
				} 
				
			}, 
			listen:function(evt){  
				if(w_wgl.camera){
					var _dir = w_wgl.camera.getWorldDirection();
					var _pos = w_wgl.camera.position;
					var _ball = undefined;
					var _ball_find = false;
					for(var _idx in _balls){
						_ball = _balls[_idx];
						if(!_ball.visible){
							_ball_find = true;
							break;
						}
					}

					if(!_ball_find){
						_ball = _ball_instance.clone(); 
						_balls.push(_ball);
						w_wgl.scene.add(_ball);
					} 
					_ball._cin1 = {};
					_ball._cin1.v0 = 700/1000;
					_ball._cin1.v0_def = 700/1000;
					_ball._cin1.vf = 0;
					_ball._cin1.vf_def = 0;
					_ball._cin1.acc = 900/1000;
					_ball.visible = true;
					_ball.position.copy(_pos).add(_dir.clone().normalize().multiplyScalar(_ball_distance_camera));
					_ball._dir = _dir.clone().normalize(); 

				}
			}, 
			render:function(){ 
				for(var _idx in _balls){
					var _ball = _balls[_idx];
					if(_ball.visible){
						_ball.position.x +=_ball._dir.x*_ball._cin1.vf;
						_ball.position.y +=_ball._dir.y*_ball._cin1.vf;
						_ball.position.z +=_ball._dir.z*_ball._cin1.vf;
						_ball._cin1.vf = _ball._cin1.v0 + _ball._cin1.acc*_dt;
						_ball._cin1.v0 = _ball._cin1.vf;

						if(_ball.position.distanceTo(w_wgl.camera.position)>100 || _ball.position.y<0){
							_ball.visible = false;
						}
					}

					if(_ball.visible)this.checkCollisions(_ball);

					/*if(_ball.visible && _lib_bm2){
						for(var _bb_idx in _lib_bm2){
							var _bb = _lib_bm2[_bb_idx];
							if(!_ball.visible || !_ball.geometry.boundingSphere || !_bb.geometry.boundingSphere)break;
							if( 
								_ball.position.distanceTo(_bb.position) <= (_bb.geometry.boundingSphere.radius + _ball.geometry.boundingSphere.radius)
							){
								_ball.visible = false;
							}
						}
					}
					if(_ball.visible && _lib_bm3){
						for(var _bb_idx in _lib_bm3){
							var _bb = _lib_bm3[_bb_idx];
							if(!_ball.visible || !_ball.geometry.boundingSphere || !_bb.geometry.boundingSphere)break;
							if( 
								_ball.position.distanceTo(_bb.position) <= (_bb.geometry.boundingSphere.radius + _ball.geometry.boundingSphere.radius)
							){
								_ball.visible = false;
							}
						}
					}*/
				}
			}, 
			checkCollisions:function(_ball){ 
				this.checkCollisionsWithBlocks(_ball);
			}, 
			checkCollisionsWithBlocks:function(_ball){
				if(!_ball.visible)return;
				if(_lib_spheres_blocks){
					for(var _idx in _lib_spheres_blocks){  
						if(!_ball.visible)break;
						var _sphere = _lib_spheres_blocks[_idx];
						if(this.sphereMeshCollisionSphere(_sphere,_ball)){
							if(_sphere._lib_objs){
								for(var _idy in _sphere._lib_objs){
									if(!_ball.visible)break;
									var _block = _sphere._lib_objs[_idy];  
									if(this.meshMeshCollisionSphere(_block,_ball)){
										for(var _idz in _block._lib_triangles){
											if(!_ball.visible)break;
											var _triangle = _block._lib_triangles[_idz]; 
											if(_triangle.boundingSphere.middle.distanceTo(_ball.position)<=(_triangle.boundingSphere.radius + _ball.geometry.boundingSphere.radius)){ 
												for(var _idn in _triangle._lib_lines3){
													if(!_ball.visible)break;
													var _line3 = _triangle._lib_lines3[_idn]; 
													if(this.line3SphereIntersecction(_line3,_ball)){
														_ball.visible = false;
													}
												} 
												if(_ball.visible){ 
													var _p1 = _triangle._lib_plane.projectPoint(_ball.position);
													if(_triangle.containsPoint(_p1)){
														var _d1 = _triangle._lib_plane.distanceToPoint(_ball.position);
														if(_d1<_ball.geometry.boundingSphere.radius){
															_ball.visible = false;
														}else{ 
															var _vec = _ball.position.clone().sub(_triangle.boundingSphere.middle);
															var _sign = Math.sign(_vec.dot(_triangle._lib_plane.normal));
															if(!_ball._lib_last_orientation){
																_ball._lib_last_orientation = _sign;
																_ball._lib_last_block_triangle_plane_uuid = _triangle._lib_plane.uuid;
															} 
															if(_sign!=_ball._lib_last_orientation){
																if(_ball._lib_last_block_triangle_plane_uuid == _triangle._lib_plane.uuid){
																	_ball.visible = false;
																}
															}else{
																_ball._lib_last_block_triangle_plane_uuid = _triangle._lib_plane.uuid;
															}
														}	
													} 
												}	
											}
										} 
									}	
								}
							}
						}
					}
				}
			},
			line3SphereIntersecction:function(line3,obj){
				var _r2 = 0;
				var _o = line3.start;
				var _c = 0;
				var _l = line3._lib_vunit;
				if(obj instanceof THREE.Sphere){
					_r2 = Math.pow(sphere.radius,2);
					_c = sphere.center.clone();
				}else if(obj instanceof THREE.Mesh){
					_r2 = Math.pow(obj.geometry.boundingSphere.radius,2);
					_c = obj.geometry.boundingSphere.center.clone().add(obj.position);
				}else{
					return false;
				}

				var _oc1 = _c.clone().sub(_o);
				_oc1 = Math.pow(_oc1.x,2)+Math.pow(_oc1.y,2)+Math.pow(_oc1.z,2);

				var _oc2 = _c.clone().sub(_o);
				_oc2 = _oc2.dot(_l);
				_oc2 = Math.pow(_oc2,2);

				var _result = _oc2 - _oc1 + _r2;
				if(_result>=0)return true;
			},
			sphereMeshCollisionSphere:function(sphere,mesh){
				return mesh.geometry.boundingSphere && sphere.center.distanceTo(mesh.position)<=(sphere.radius+mesh.geometry.boundingSphere.radius);
			},
			meshMeshCollisionSphere:function(mesh1,mesh2){

				if(
					mesh1.geometry.boundingSphere && 
					mesh2.geometry.boundingSphere 
				){
					var _sum = mesh1.geometry.boundingSphere.radius + mesh2.geometry.boundingSphere.radius;
					return mesh1.position.distanceTo(mesh2.position) <= _sum;
				}
				return false;
			},
			resize:function(){ 
			} 
		};

		return {
			instance:function(_p){
				if(!_instance){
					_instance = new Shooter(_p);
					return _instance;
				}
				return null;
			}
		};
	}
)();
